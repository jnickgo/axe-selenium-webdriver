plugins {
    java
}

group = "com.nickgover"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {

    compile("com.deque:axe-selenium:2.1")
    compile("com.codeborne:selenide:5.1.0")
    compile("org.json:json:20180813")
    testCompile("org.junit.jupiter:junit-jupiter-api:5.3.2")
    testCompile("org.junit.jupiter:junit-jupiter-params:5.3.2")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:5.3.2")


}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}