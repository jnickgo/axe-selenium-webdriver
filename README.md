# aXe Selenium WebDriver (Java/Selenide)
An exploratory sample application that utilized Selenium WebDriver and aXe for automated accessibility testing.

## Contents
* What is aXe?
* How to use this project
* Selenide


## What is aXe?
Axe is an accessibility testing engine for websites and other HTML-based user interfaces. It's fast, secure, lightweight, and was built to seamlessly integrate with any existing test environment so you can automate accessibility testing alongside your regular functional testing.

**Why use Axe?**
* Axe is open source.
* It returns zero false positives (bugs notwithstanding).
* It's designed to work on all modern browsers and with whatever tools, frameworks, libraries and environments you use today.
* It's actively supported by Deque Systems, a major accessibility vendor.
* It integrates with your existing functional/acceptance automated tests.
* It automatically determines which rules to run based on the evaluation context.
* Axe supports in-memory fixtures, static fixtures, integration tests and iframes of infinite depth.
* Axe is highly configurable.

**aXe Core**
* [Source code](https://github.com/dequelabs/axe-core)


## How to use this project
Axe is natively available and ran as an npm package.  This project uses a Java Selenium wrapper
to execute the core scripts for Axe.

### To run tests
From the command line simply run `gradle(w) --build` or `grade(w) --verification` to run the tests.

Reports are generated and output to the build/reports/tests/test/ directory.

## Selenide
Selenide is a wrapper for Selenium WebDriver that allows you easier and faster writing of UI Tests. With Selenide you can concentrate on business logic instead of solving all these endless browser/ajax/timeouts problems.



