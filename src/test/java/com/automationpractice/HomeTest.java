package com.automationpractice;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.deque.axe.AXE;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.net.URL;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Validate accessibilty errors on the home page.
 */
public class HomeTest {

    private String testName;
    private static final URL scriptUrl = HomeTest.class.getResource("/axe.min.js");
    private String appUrl = "https://www.nipr.com/";

    @BeforeEach
    public void setup(TestInfo testInfo) {
        testName = testInfo.getDisplayName();
        Configuration.browser = "chrome";
        Configuration.timeout = 10000;
        open(appUrl);
    }

    @Test
    public void reportAllFindings() {


        JSONObject responseJSON = new AXE.Builder(WebDriverRunner.getWebDriver(), scriptUrl)
                .options("{ rules: { 'accesskeys': { enabled: false } } }")
                .analyze();

        JSONArray violations = responseJSON.getJSONArray("violations");

        if (violations.length() == 0) {
            assertTrue(true, "No violations found");
        } else {
            AXE.writeResults(testName, responseJSON);

            assertTrue(false, AXE.report(violations));
        }
    }


    @Test
    public void findCriticalViolations() {

        JSONObject responseJSON = new AXE.Builder(WebDriverRunner.getWebDriver(), scriptUrl).analyze();

        JSONArray violations = responseJSON.getJSONArray("violations");

        if (violations.length() == 0) {
            assertTrue(true, "No violations found");
        } else {
            AXE.writeResults(testName, responseJSON);
            fail(AXE.report(violations));
        }

    }
}
